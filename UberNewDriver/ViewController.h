//
//  ViewController.h
//  UberNewDriver
//
//  Created by Elluminati on 27/09/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController : BaseVC <CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblCopyrights;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;

- (IBAction)onClickRegister:(id)sender;

@end